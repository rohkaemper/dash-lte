#include "dash-server-helper.h"
#include "dash-server.h"
#include "ns3/inet-socket-address.h"
#include "ns3/packet-socket-address.h"
#include "ns3/uinteger.h"
#include "ns3/names.h"
#include "ns3/string.h"

namespace ns3 {

  DashServerHelper::DashServerHelper (Address local, uint16_t port, std::string protocol, DataRate cbr) {
    m_factory.SetTypeId (DashServer::GetTypeId ());
    SetAttribute ("Local", AddressValue (local));
    SetAttribute ("ListenPort", UintegerValue (port));
    SetAttribute ("Protocol", StringValue (protocol));
    SetAttribute ("DataRate", DataRateValue (cbr));
  }

  void DashServerHelper::SetAttribute (std::string name, const AttributeValue &value) {
    m_factory.Set (name, value);
  }

  ApplicationContainer DashServerHelper::Install (Ptr<Node> node) const {
    return ApplicationContainer (InstallPriv (node));
  }

  ApplicationContainer DashServerHelper::Install (std::string nodeName) const {
    Ptr<Node> node = Names::Find<Node> (nodeName);
    return ApplicationContainer (InstallPriv (node));
  }

  ApplicationContainer DashServerHelper::Install (NodeContainer nodes) const {
    ApplicationContainer apps;
    for (NodeContainer::Iterator i = nodes.Begin (); i != nodes.End (); ++i) {
      apps.Add (InstallPriv (*i));
    }
    return apps;
  }

  Ptr<Application> DashServerHelper::InstallPriv (Ptr<Node> node) const {
    Ptr<Application> app = m_factory.Create<Application> ();
    node->AddApplication (app);

    return app;
  }

}
