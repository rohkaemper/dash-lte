#include "dash-client.h"
#include "ns3/log.h"
#include "ns3/socket.h"
#include "ns3/socket-factory.h"
#include "ns3/tcp-socket-factory.h"
#include <random>
#include <fstream>
#include <string>

namespace ns3 {
  NS_LOG_COMPONENT_DEFINE ("DashClient");
  NS_OBJECT_ENSURE_REGISTERED (DashClient);

  TypeId DashClient::GetTypeId (void) {
    static TypeId tid = TypeId ("ns3::DashClient")
    .SetParent<Application> ()
    .SetGroupName("Applications")
    .AddConstructor<DashClient> ()
    .AddAttribute ("Protocol",
                   "The TypeId of the protocol to use for the rx socket",
                   TypeIdValue (TcpSocketFactory::GetTypeId ()),
                   MakeTypeIdAccessor (&DashClient::m_tid),
                   MakeTypeIdChecker ())
    .AddAttribute ("RemoteAddress",
                   "The destination Address ot the server.",
                   AddressValue (),
                   MakeAddressAccessor (&DashClient::m_remoteAddress),
                   MakeAddressChecker ())
    .AddAttribute ("RemotePort",
                   "The destination port of the server",
                   UintegerValue (0),
                   MakeUintegerAccessor (&DashClient::m_serverPort),
                   MakeUintegerChecker<uint16_t>())
    .AddAttribute ("DataRate", "The data rate in on state.",
                   DataRateValue (DataRate ("5Mbps")),
                   MakeDataRateAccessor (&DashClient::m_cbrRate),
                   MakeDataRateChecker ())
    .AddTraceSource ("Rx",
                     "Packet received",
                     MakeTraceSourceAccessor (&DashClient::m_rxTrace),
                     "ns3::Packet::AddressTracedCallback")
    .AddTraceSource ("Tx",
                     "Request sent.",
                     MakeTraceSourceAccessor (&DashClient::m_txTrace),
                     "ns3::Packet::AddressTracedCallback")
    ;
    return tid;
  }

  DashClient::DashClient (void) {
    NS_LOG_FUNCTION (this);
    m_active = true;
    m_eofSent = false;
    m_playing = false;
    m_totalRx = 0;
    m_currentRequest = 0;
    m_totalRequestedBytes = 0;
    m_totalVideoBytes = 0;
    m_socket = 0;
    m_serverPort = 80;
    m_sentRequests = 0;
    m_activeRequests = 0;
    m_maxConcurrentRequests = 2;
    m_bufferIndex = 0;
    m_framerate = 24;
    m_currentQuality = 10;
    m_maxQuality = 20;
    m_videoFragmentDuration = 2;
    m_lastVideoPlaybackBuffer = 0.;
    // qfactor (Up/Down scaling) is a multiplicator to videoFragmentDuration. 1.0 adapts if difference in Buffer ist larger or equal 1 Fragment duration. Higher values adapt slower, lower adapt faster!
    m_qfactorUp = 0.7;
    m_qfactorDown = 0.5;
    m_negativeCumulatedDifference = 0.;

    m_stalls = 0;
    m_frameBufferLow = 5.0;
    m_frameBufferFull = 20.0;
    m_headerRemoved = false;
  }

  DashClient::~DashClient (void) {
    NS_LOG_FUNCTION (this);

  }

  void DashClient::DoDispose (void) {
    NS_LOG_FUNCTION_NOARGS ();
    Application::DoDispose ();

  }

  void DashClient::StartApplication (void) {
    if (m_socket == 0) {
      TypeId tid = TypeId::LookupByName ("ns3::TcpSocketFactory");
      m_socket = Socket::CreateSocket (GetNode (), tid);
      m_socket->SetAttribute("SegmentSize", UintegerValue(1448));
      // m_socket->SetAttribute("RcvBufSize", UintegerValue(UINT32_MAX));
      m_socket->SetCloseCallbacks (MakeCallback (&DashClient::HandlePeerClose, this), MakeCallback (&DashClient::HandlePeerError, this));
      m_socket->SetRecvCallback (MakeCallback (&DashClient::HandleRead, this));

      // connect == 0 -> Success
      int established = m_socket->Connect (m_remoteAddress);
      if (established == 0) {
        NS_LOG_DEBUG ("Establishing connection to server: " << GetAddressFromSocket(m_socket).GetIpv4 () << ":"
                      << GetAddressFromSocket(m_socket).GetPort ());
      // m_socket->SetAttribute("SndBufSize", UintegerValue(UINT32_MAX));
      // Schedule First Reqeust after start of Application
        ScheduleTransmit ();
      } else {
        NS_LOG_WARN ("Cannot establish connection to server.");
      }
      // ~6 Mbit/s / 565MByte --> 95s at 50Mbps
      // readVideoInfo("scratch/dash-lte/video-info/ToS-1080p.txt");
      // ~8Mbit/s / ~722MByte --> 121s at 50Mbps
      // readVideoInfo("scratch/dash-lte/video-info/ToS-4k-1080p_scaled.txt");
      // ~78 Mbit/s / ~6.7GByte --> 1116s at 50 Mbps
      readVideoInfo("scratch/dash-lte/video-info/tearsofsteel_4k.txt");
    }
    // Headings for log output, will be recognized by 'awk'
    NS_LOG_DEBUG ("VIDEO:Framesize, Framequality, Frameindex, FrameTimestamp");
    NS_LOG_DEBUG ("FRAGMENT:SimulationTime, FragmentSize, FragmentQuality");
    NS_LOG_DEBUG ("PBUFFER:SimulationTime, BufferedVideoSeconds, BufferedVideoQuality");
    NS_LOG_DEBUG ("PSTALL:SimulationTime, StallingEvent");
    NS_LOG_DEBUG ("BUFFDIFF:SimulationTime, BufferedVideoSecondsDifference");
    NS_LOG_DEBUG ("VPLAYBACK:SimulationTime, VideoTimestamp");

  }

  void DashClient::StopApplication (void) {
    if (m_active) {
      NS_LOG_DEBUG ("Stopped Application");
      // Simulator::Cancel (m_checkEvent);
      Simulator::Cancel (m_sendEvent);
      Simulator::Cancel (m_playbackEvent);
      m_socket->ShutdownSend ();
      m_active = false;
      m_socket->Close ();
      NS_LOG_DEBUG ("Local socket closed. \nCLIENT RX: " << m_totalRx << " bytes \nREQUESTED VIDEO " << m_totalRequestedBytes << " bytes\n RECEIVED VIDEO " << m_totalVideoBytes << " bytes\nStalls: " << m_stalls);
      Simulator::Stop (Seconds (1.0));
    }

  }

  void DashClient::HandleRead (Ptr<Socket> socket) {
    Ptr<Packet> packet;
    Address from;
    TcpHeader header;

    while ((packet = socket->RecvFrom (from))) {
      if (packet->GetSize () <= 0) {
        //EOF
        NS_LOG_WARN ("Empty packet received!");
        break;
      }
      m_totalRx += packet->GetSize ();
      if (!m_headerRemoved) {
        packet->RemoveHeader (header);
        m_headerRemoved = true;
      }
      m_currentRequest += packet->GetSize ();
      m_totalVideoBytes += packet->GetSize ();
      // NS_LOG_DEBUG ("Current request: " << m_currentRequest << "/" << GetCurrentSegment ());
      struct VideoSegment segmentRequest = GetCurrentSegment ();
      if (m_currentRequest >= segmentRequest.size) {
        m_activeRequests--;
        ResetCurrentRequest(m_currentRequest);
        m_headerRemoved = false;
        UpdateFrameBuffer(GetCurrentSegment ());
        if (RemoveCurrentFragmentRequest ()) {
          NS_LOG_FUNCTION ("Successfully removed fragment from request list.");
        } else {
          NS_LOG_WARN ("Error while removing fragment from request list!");
        }
        if (m_active) {
          ScheduleTransmit ();
        } else {
          StopApplication ();
        }
      }
      if (InetSocketAddress::IsMatchingType (from)) {
        // NS_LOG_DEBUG ("At time " << Simulator::Now ().GetSeconds().GetSeconds ()
        //            << "s 'Dash Server' received "
        //            <<  packet->GetSize () << " bytes from "
        //            << InetSocketAddress::ConvertFrom(from).GetIpv4 ()
        //            << " port " << InetSocketAddress::ConvertFrom (from).GetPort ()
        //            << " total Rx " << m_totalRx << " bytes");
      } else if (Inet6SocketAddress::IsMatchingType (from)) {
        // NS_LOG_DEBUG ("At time " << Simulator::Now ().GetSeconds().GetSeconds ()
        //            << "s 'Dash Server' received "
        //            <<  packet->GetSize () << " bytes from "
        //            << Inet6SocketAddress::ConvertFrom(from).GetIpv6 ()
        //            << " port " << Inet6SocketAddress::ConvertFrom (from).GetPort ()
        //            << " total Rx " << m_totalRx << " bytes");
      }
      m_rxTrace (packet, from);
    }

  }

  void DashClient::HandlePeerClose (Ptr<Socket> socket) {
    NS_LOG_DEBUG ("Connection closed by server.");
    // TODO: Move Stop Application to end of video instead of peer closing...
    StopApplication ();
  }

  void DashClient::HandlePeerError (Ptr<Socket> socket) {
    NS_LOG_WARN (this << socket);
    NS_LOG_DEBUG ("Client had an error...");

  }

  void DashClient::ScheduleTransmit (void) {
    NS_LOG_FUNCTION ("Scheduling request in simulation environment.");
    struct VideoSegment segmentRequest = GetCurrentSegment ();
    uint32_t bits = (segmentRequest.size - m_currentRequest) * 8;
    Time tNext = (Seconds (bits / static_cast<double>(m_cbrRate.GetBitRate ())));
    if (m_activeRequests > m_maxConcurrentRequests) {
      // Time tNext (Seconds (0.1));
      if (tNext.Compare(Seconds (0.1)) < 0) {
        tNext = Seconds (0.1);
      }
    } else {
      tNext = Seconds (0.1);
    }
    if (m_active) {
      m_sendEvent = Simulator::Schedule (tNext, &DashClient::SendPacket, this);
    }
  }

  void DashClient::SendPacket (void) {
    if ((m_activeRequests < m_maxConcurrentRequests) && (!m_videoInfo.empty ()) && m_active) {
      std::ostringstream payloadstr;
      // uncomment next line to add random payload.
      // uint32_t payload = random_payload();
      // use predefined values from file
      uint32_t payload = CalculateFragmentSize ();
      payloadstr << std::to_string(payload) << '\0';
      Ptr<Packet> packet = Create<Packet> ((uint8_t*) payloadstr.str().c_str(), payloadstr.str().length());
      // Adding TCP Header
      TcpHeader header;
      packet->AddHeader (header);
      AddFragmentRequest (payload, m_currentQuality);
      m_totalRequestedBytes += payload;
      uint32_t status = m_socket->Send (packet);
      NS_LOG_FUNCTION ("Now requesting segment '" << m_sentRequests+1 << "' of size " << payload << " bytes. Status: " << status);
      ScheduleTransmit ();
      m_activeRequests++;
      m_sentRequests++;
    } else {
      if (m_videoInfo.empty () && !m_eofSent) {
        m_eofSent = true;
        SendEOF ();
      }
    }
  }

  void DashClient::SendEOF (void) {
    std::ostringstream payloadstr;
    payloadstr << "EOF" << '\0';
    Ptr<Packet> packet = Create<Packet> ((uint8_t*) payloadstr.str().c_str(), payloadstr.str().length());
    TcpHeader header;
    packet->AddHeader(header);
    m_socket->Send (packet);
    NS_LOG_DEBUG ("EOF reached. Server informed!");
    // StopApplication ();
    m_socket->ShutdownSend ();
  }

  uint32_t DashClient::random_payload (void) {
    std::random_device rd;     // only used once to initialise (seed) engine
    std::mt19937 rng(rd());    // random-number engine used (Mersenne-Twister in this case)
    // std::uniform_int_distribution<uint32_t> uni(5000,131012); // guaranteed unbiased
    std::uniform_int_distribution<uint32_t> uni(5000,2147836); // guaranteed unbiased
    // std::uniform_int_distribution<uint32_t> uni(1,4294967295); // guaranteed unbiased this is uint32_t_max

    return uni(rng);
  }

  VideoSegment DashClient::GetCurrentSegment (void) {
    return m_requestedSegment.empty ()? (VideoSegment) {static_cast<uint32_t> (0),static_cast<uint16_t> (0)}: (VideoSegment) {m_requestedSegment.front().size, m_requestedSegment.front().quality};
  }

  void DashClient::PrintRequests (void) {
    NS_LOG_DEBUG ("Current Fragment Requests:");
    for (auto segment : m_requestedSegment) {
      NS_LOG_DEBUG("size: " << segment.size << " quality: " << segment.quality);
    }
  }

  void DashClient::AddFragmentRequest (uint32_t segment, uint16_t quality) {
    struct VideoSegment tmp;
    tmp.size = segment;
    tmp.quality = quality;
    NS_LOG_FUNCTION ("Added: " << tmp.size << " bytes with quality: " << tmp.quality);
    m_requestedSegment.push_back(tmp);
    NS_LOG_DEBUG ("FRAGMENT:" << Simulator::Now ().GetSeconds() << ", " << tmp.size << ", " << tmp.quality);
  }

  bool DashClient::RemoveCurrentFragmentRequest (void) {
    if (!m_requestedSegment.empty ()) {
      struct VideoSegment segmentRequest = GetCurrentSegment ();
      NS_LOG_FUNCTION ("Removing Fragment: " << segmentRequest.size);
      int listSize = m_requestedSegment.size ();
      m_requestedSegment.pop_front();
      // PrintRequests ();
      if (m_requestedSegment.size () < listSize) {
        return true;
      } else {
        return false;
      }
    } else {
      NS_LOG_WARN ("Operating on empty list!");
      return false;
    }
  }

  void DashClient::ResetCurrentRequest (uint32_t size) {
    struct VideoSegment segmentRequest = GetCurrentSegment ();
    m_currentRequest = size - segmentRequest.size;
  }

  InetSocketAddress DashClient::GetAddressFromSocket (Ptr<Socket> socket) {
    Address addr;

    socket->GetPeerName (addr);
    return InetSocketAddress::ConvertFrom (addr);
  }

  void DashClient::UpdateFrameBuffer (VideoSegment segmentRequest) {
    // Maybe not optimal here. Move if needed!
    // Check if we have to adapt the quality!
    CalculateBufferDifference ();

    NS_LOG_FUNCTION ("Adding frames to videobuffer (size/quality) -> " << segmentRequest.size << "/" << segmentRequest.quality);
    uint32_t endOfFragment = m_bufferIndex + (m_videoFragmentDuration * m_framerate);
    while (m_bufferIndex < endOfFragment) {
      AddFrame(m_bufferIndex++);
    }
    CheckPlaybackBuffer ();
    // uncomment to print frame buffer contents after new fragment was added.
    // PrintList();
  }

  void DashClient::AddFrame(uint32_t index) {
    VideoSegment curr = GetCurrentSegment ();
    // time = (index / framerate)[s] + ((index modulo framerate) / framerate)[s]
    double time = static_cast<double> (index / m_framerate) + (static_cast<double> (index % m_framerate) / m_framerate);
    uint32_t size = static_cast<uint32_t>((curr.size/m_framerate)*static_cast<double>(curr.quality)/m_maxQuality);
    NS_LOG_DEBUG ("VIDEO:" << size << ", " << curr.quality << ", " << index << ", " << time);
    // size , quality, index, timestamp
    m_frameList.push_back((Frame) {size, curr.quality, index, time});
  }

  void DashClient::PrintList (uint16_t list) {
    if (list == 1) {
      for (auto frame : m_frameList) {
        NS_LOG_DEBUG("index: " << frame.index << " time: " << static_cast<double> (frame.timestamp) << " size: " << frame.size << " quality: " << frame.quality );
      }
    } else {
      for (auto frame : m_videoInfo) {
        NS_LOG_DEBUG("size: " << frame.size << " quality: " << frame.quality);
      }
    }
  }

  void DashClient::readVideoInfo (const char *filename) {
    NS_LOG_DEBUG ("Reading video information from file: '" << filename << "'.");
    std::string line;
    std::ifstream file(filename);
    while (std::getline (file, line)) {
      std::string time;
      std::string size;
      std::istringstream linestream(line);
      getline (linestream, time, ',');
      getline (linestream, size, ',');
      uint32_t frameSize = std::atoi(size.c_str());
      m_videoInfo.push_back((VideoSegment) {frameSize, m_maxQuality});
    }
    NS_LOG_DEBUG("index: " << m_videoInfo.size () << " duration: " << (m_videoInfo.size ()/static_cast<double>(m_framerate)) << "s   " << (m_videoInfo.size ()/static_cast<double>(m_framerate)/60) << "min");
    // PrintList (2);
  }

  uint32_t DashClient::CalculateFragmentSize (void) {
    uint32_t fragmentSize = 0;
    VideoSegment frame;
    if (m_videoInfo.size () >= (m_framerate * m_videoFragmentDuration)) {
      for (int i = 0; i < (m_framerate * m_videoFragmentDuration); i++) {
        frame = m_videoInfo.front ();
        fragmentSize += frame.size;
        m_videoInfo.pop_front ();
      }
    } else {
      int length = m_videoInfo.size ();
      for (int i = 0; i < length; i++) {
        frame = m_videoInfo.front ();
        fragmentSize += frame.size;
        m_videoInfo.pop_front ();
      }
    }
    double scaledFramgmentSize = static_cast<double>(fragmentSize) * (static_cast<double>(m_currentQuality) / m_maxQuality);
    return static_cast<uint32_t>(scaledFramgmentSize);
  }

  void DashClient::CheckPlaybackBuffer (void) {
    NS_LOG_FUNCTION ("Checking playback buffer...");
    double bufferedVideoInSeconds = static_cast<double>(m_frameList.size ()) / static_cast<double>(m_framerate);
    NS_LOG_DEBUG ("PBUFFER:" << Simulator::Now ().GetSeconds() << ", " << bufferedVideoInSeconds << ", " << m_frameList.front ().quality);
    if (bufferedVideoInSeconds != 0.) {
      // Reduce logging output
      if (std::fmod(bufferedVideoInSeconds,1.0) == 0.) {
        NS_LOG_FUNCTION ("Buffered data for " << bufferedVideoInSeconds << " seconds of playback. (frames: " << m_frameList.size() << " currentQuality: " << m_frameList.front().quality << ")");
      }
      if (!m_playing && (bufferedVideoInSeconds >= m_frameBufferLow)) {
        PlayVideo ();
      }
      if (bufferedVideoInSeconds > m_frameBufferFull) {
        // Stop requesting until buffer reached lower limit
        Simulator::Cancel (m_sendEvent);
      } else if ((bufferedVideoInSeconds <= m_frameBufferLow) && m_sendEvent.IsExpired ()) {
        // Start requesting again
        SendPacket ();
        m_lastVideoPlaybackBuffer = m_frameBufferLow;
      }
    } else {
      if ((m_sentRequests > 0) && !m_eofSent) {
        NS_LOG_WARN ("No video buffered, stalling event.");
        NS_LOG_DEBUG ("PSTALL:" << Simulator::Now ().GetSeconds() << ", " << "0");
        m_stalls++;
        StopVideo ();
      } else if ((m_sentRequests > 0) && m_eofSent) {
        NS_LOG_DEBUG ("Reached end of video. :)");
        StopVideo ();
        StopApplication ();
      } else {
        NS_LOG_DEBUG ("Buffer checked too early. No requests sent until now.");
      }
    }
  }

  void DashClient::PlayVideo (void) {
    if (!m_playing) {
      NS_LOG_DEBUG ("Starting video.");
      m_playing = true;
      RemoveVideoFrame ();
    }
  }

  void DashClient::StopVideo (void) {
    if (m_playing) {
      NS_LOG_DEBUG ("Stopped video.");
      Simulator::Cancel (m_playbackEvent);
      m_playing = false;
    }
  }

  void DashClient::RemoveVideoFrame (void) {
    // NS_LOG_DEBUG ("playing...");
    if (!m_frameList.empty ()) {
      NS_LOG_DEBUG ("VPLAYBACK:" << Simulator::Now ().GetSeconds () << ", " << m_frameList.front ().timestamp);
      m_frameList.pop_front ();
      m_playbackEvent = Simulator::Schedule (Seconds (1.0/m_framerate), &DashClient::RemoveVideoFrame, this);
    } else {
      StopVideo ();
    }
    CheckPlaybackBuffer ();
  }

  void DashClient::CalculateBufferDifference (void) {
    double currentVideoPlaybackBuffer = static_cast<double>(m_videoFragmentDuration) + static_cast<double>(m_frameList.size ()) / static_cast<double>(m_framerate);
    double diff = currentVideoPlaybackBuffer - m_lastVideoPlaybackBuffer;
    if (diff < 0.) {
      m_negativeCumulatedDifference += diff;
      NS_LOG_DEBUG ("BUFFDIFF:" << Simulator::Now ().GetSeconds() << ", " << m_negativeCumulatedDifference);
      if (m_negativeCumulatedDifference < (static_cast<double>(m_videoFragmentDuration) * m_qfactorUp * -1.0)) {
        // Check if we need to lower quality drastically (take qfactorUp as reference) and then half requested quality.
        if (m_currentQuality - (m_maxQuality / 2) > 1) {
          m_currentQuality -= m_maxQuality / 2;
        } else if (m_currentQuality > 1) {
          m_currentQuality--;
        }
        NS_LOG_FUNCTION ("Critical quality decrease to '" << m_currentQuality << "'.");
        m_negativeCumulatedDifference = 0.;
      } else if (m_negativeCumulatedDifference < static_cast<double>(m_videoFragmentDuration) * m_qfactorDown * -1.0) {
        // lower quality demand if possible
        if (m_currentQuality > 1) {
          NS_LOG_FUNCTION ("Quality decreased.");
          m_currentQuality--;
          m_negativeCumulatedDifference = 0.;
        }
      }
    } else {
      // check if we can raise quality?
      NS_LOG_DEBUG ("BUFFDIFF:" << Simulator::Now ().GetSeconds() << ", " << diff);
      if (diff >= (static_cast<double>(m_videoFragmentDuration) * m_qfactorUp)) {
        if (m_currentQuality < m_maxQuality) {
          NS_LOG_FUNCTION ("Quality increased.");
          m_currentQuality++;
        }
      }
      m_negativeCumulatedDifference = 0.;
    }
    // NS_LOG_DEBUG ("Buffer Difference: " << diff << "s; " << " negative Difference: "<< m_negativeCumulatedDifference << "s");
    m_lastVideoPlaybackBuffer = currentVideoPlaybackBuffer;
  }

} // End of namespace
