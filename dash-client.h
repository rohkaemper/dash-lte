#ifndef DASH_CLIENT_H
#define DASH_CLIENT_H

#include "ns3/address.h"
#include "ns3/applications-module.h"
#include "ns3/ptr.h"
#include "ns3/traced-callback.h"
#include "ns3/tcp-header.h"
#include "ns3/data-rate.h"
#include "ns3/inet-socket-address.h"
#include <tuple>
#include <cstdint>

namespace ns3 {

  struct VideoSegment {
    uint32_t size;
    uint16_t quality;
  };

  struct Frame {
    uint32_t size;
    uint16_t quality;
    uint64_t index;
    double timestamp;
  };

  class Socket;
  class Packet;

  class DashClient : public Application {

  public:
    static TypeId GetTypeId (void);
    DashClient (void);
    virtual ~DashClient (void);

    // void CalculateAvailableBandwidth();
    // void CheckBuffers();
    // void CheckDeltaInBuffer();
    // void LogSignificantData();
    // void PlayBackVideo();
    // void PutVideoFragmentToBuffer();
    // void ReceiveVideoFragment();
    // void RequestVideoFragment();
    // void SetupClient();

  protected:
    virtual void DoDispose (void);

  private:
    virtual void StartApplication (void);
    virtual void StopApplication (void);
    void HandleRead (Ptr<Socket>);
    void HandlePeerClose (Ptr<Socket>);
    void HandlePeerError (Ptr<Socket>);
    void ScheduleTransmit (void);
    void SendPacket (void);
    void SendEOF (void);
    uint32_t random_payload (void);
    VideoSegment GetCurrentSegment (void);
    bool RemoveCurrentFragmentRequest (void);
    void AddFragmentRequest (uint32_t, uint16_t);
    void PrintRequests (void);
    void ResetCurrentRequest (uint32_t);
    void UpdateFrameBuffer (VideoSegment);
    InetSocketAddress GetAddressFromSocket (Ptr<Socket>);
    void AddFrame (uint32_t);
    void PrintList (uint16_t);
    void readVideoInfo (const char *);
    uint32_t CalculateFragmentSize (void);
    void CheckPlaybackBuffer (void);
    void PlayVideo (void);
    void StopVideo (void);
    void RemoveVideoFrame (void);
    void CalculateBufferDifference (void);

    bool            m_active;
    bool            m_eofSent;
    bool            m_playing;
    uint64_t        m_totalRx;
    uint64_t        m_totalVideoBytes;
    uint64_t        m_totalRequestedBytes;
    uint64_t        m_sentRequests;
    uint64_t        m_activeRequests;
    uint64_t        m_maxConcurrentRequests;
    TypeId          m_tid;
    Ptr<Socket>     m_socket;
    Address         m_remoteAddress;
    uint16_t        m_serverPort;
    DataRate        m_cbrRate;
    EventId         m_sendEvent;
    EventId         m_playbackEvent;
    int             m_currentRequest;
    bool            m_headerRemoved;
    // VideoBuffer
    uint32_t        m_bufferIndex;
    uint16_t        m_framerate;
    uint16_t        m_currentQuality;
    uint16_t        m_maxQuality;
    uint16_t        m_videoFragmentDuration;
    std::list<VideoSegment> m_videoInfo;
    std::list<VideoSegment> m_requestedSegment;
    std::list<Frame>  m_frameList;
    double          m_lastVideoPlaybackBuffer;
    double          m_negativeCumulatedDifference;
    // Upper and lower Treshold for VideoBuffer
    double          m_frameBufferLow;
    double          m_frameBufferFull;
    double          m_qfactorUp;
    double          m_qfactorDown;

    // Statistics
    uint16_t        m_stalls;

    TracedCallback<Ptr<const Packet>, const Address &> m_rxTrace;
    TracedCallback<Ptr<const Packet>, const Address &> m_txTrace;
  };

} // End of namespace
#endif
