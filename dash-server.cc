#include "dash-server.h"
#include "ns3/log.h"
#include "ns3/socket.h"
#include "ns3/tcp-socket-factory.h"

namespace ns3 {
  NS_LOG_COMPONENT_DEFINE ("DashServer");
  NS_OBJECT_ENSURE_REGISTERED (DashServer);

  TypeId DashServer::GetTypeId (void) {
    static TypeId tid = TypeId ("ns3::DashServer")
    .SetParent<Application> ()
    .SetGroupName("Applications")
    .AddConstructor<DashServer> ()
    .AddAttribute ("Local", "The Address on which to Bind the rx socket.",
                   AddressValue(), MakeAddressAccessor (&DashServer::m_local), MakeAddressChecker ())
    .AddAttribute ("Protocol", "The TypeId of the protocol to use for the rx socket",
                   TypeIdValue (TcpSocketFactory::GetTypeId ()), MakeTypeIdAccessor (&DashServer::m_tid),
                   MakeTypeIdChecker ())
    .AddAttribute ("ListenPort",
                   "ListenPort for connection to Server", UintegerValue (0), MakeUintegerAccessor (&DashServer::m_port),
                   MakeUintegerChecker<uint16_t>())
    .AddAttribute ("DataRate", "The data rate in on state.",
                   DataRateValue (DataRate ("5Mbps")),
                   MakeDataRateAccessor (&DashServer::m_cbrRate),
                   MakeDataRateChecker ())
    .AddTraceSource ("Rx", "Packet received", MakeTraceSourceAccessor (&DashServer::m_rxTrace),
                     "ns3::Packet::AddressTracedCallback")
    .AddTraceSource ("Tx", "Packet sent", MakeTraceSourceAccessor (&DashServer::m_txTrace),
                     "ns3::Packet::AddressTracedCallback")
    ;
    return tid;
  }

  DashServer::DashServer (void) {
    NS_LOG_FUNCTION (this);
    m_socket = 0;
    m_stopped = false;
    m_port = 80;
    m_totalRx = 0;
    m_totalTx = 0;
    std::list<uint32_t> m_requestedFragment;
  }

  DashServer::~DashServer (void) {
    NS_LOG_FUNCTION (this);
  }

  void DashServer::DoDispose (void) {
    NS_LOG_FUNCTION_NOARGS ();
    Application::DoDispose ();
  }

  void DashServer::StartApplication (void) {
    if (m_socket == 0) {
      TypeId tid = TypeId::LookupByName ("ns3::TcpSocketFactory");
      m_socket = Socket::CreateSocket (GetNode (), tid);
      m_socket->SetAttribute("SegmentSize", UintegerValue(1448));
      m_socket->SetAttribute("SndBufSize", UintegerValue(UINT32_MAX));
      // m_socket->SetAttribute("RcvBufSize", UintegerValue(UINT32_MAX));
      if (m_socket->Bind (m_local) == -1) {
        NS_FATAL_ERROR ("Failed to bind socket");
      }
      m_socket->Listen ();
      NS_LOG_DEBUG ("Listening on " << InetSocketAddress::ConvertFrom (m_local).GetIpv4() << ":"
                    << InetSocketAddress::ConvertFrom (m_local).GetPort());
    }

    m_socket->SetRecvCallback (MakeCallback (&DashServer::HandleRead, this));
    m_socket->SetAcceptCallback (MakeNullCallback <bool, Ptr<Socket>, const Address&> (), MakeCallback(&DashServer::HandleAccept, this));
    m_socket->SetCloseCallbacks (MakeCallback (&DashServer::HandlePeerClose, this), MakeCallback (&DashServer::HandlePeerError, this));

  }

  void DashServer::StopApplication (void) {
    // Close accepted sockets
    m_stopped = true;
    while(!m_socketList.empty ()) {
      Ptr<Socket> acceptedSocket = m_socketList.front ();
      m_socketList.pop_front ();
      acceptedSocket->Close ();
      NS_LOG_DEBUG ("Closed connection.");
    }
    // Close local socket
    if (m_socket != 0) {
      NS_LOG_DEBUG ("Closing local socket.");
      m_socket->Close ();
      m_socket->SetRecvCallback (MakeNullCallback<void, Ptr<Socket> > ());
      m_socket = 0;
    }
    NS_LOG_DEBUG ("Stopped Server Application.");
  }

  void DashServer::HandleRead (Ptr<Socket> socket) {
    Ptr<Packet> packet;
    Address from;

    while ((packet = socket->RecvFrom (from))) {
      if (packet->GetSize () == 0) {
        //EOF
        NS_LOG_WARN ("Empty packet received!");
        break;
      }

      m_totalRx += packet->GetSize ();
      if (InetSocketAddress::IsMatchingType (from)) {
        TcpHeader header;
        packet->RemoveHeader(header);

        uint8_t *buffer = new uint8_t[packet->GetSize ()];
        packet->CopyData (buffer, packet->GetSize ());
        std::string payload = std::string ((char*) buffer);
        NS_LOG_FUNCTION ("Received request: '" << payload << "'");
        if (payload.compare("EOF") != 0) {
          AddSegment (std::stoul (payload));
          SendSegment (GetSocket (), GetCurrentFragment ());
        } else {
          m_socket->ShutdownRecv ();
        }
        // LOGGING of a packet sink. Maybe better for client side reception.
        // NS_LOG_DEBUG ("At time " << Simulator::Now ().GetSeconds ()
        //            << "s 'Dash Server' received "
        //            <<  packet->GetSize () << " bytes from "
        //            << InetSocketAddress::ConvertFrom(from).GetIpv4 ()
        //            << " port " << InetSocketAddress::ConvertFrom (from).GetPort ()
        //            << " total Rx " << m_totalRx << " bytes");
      } else if (Inet6SocketAddress::IsMatchingType (from)) {
        // LOGGING of a packet sink. Maybe better for client side reception.
        // NS_LOG_DEBUG ("At time " << Simulator::Now ().GetSeconds ()
        //            << "s 'Dash Server' received "
        //            <<  packet->GetSize () << " bytes from "
        //            << Inet6SocketAddress::ConvertFrom(from).GetIpv6 ()
        //            << " port " << Inet6SocketAddress::ConvertFrom (from).GetPort ()
        //            << " total Rx " << m_totalRx << " bytes");
      }
      m_rxTrace (packet, from);
    }
  }

  void DashServer::HandleAccept (Ptr<Socket> socket, const Address& from) {
    NS_LOG_DEBUG ("Connection accepted from " << GetAddressFromSocket (socket).GetIpv4 () << ":" << GetAddressFromSocket (socket).GetPort () <<  ".");

    socket->SetRecvCallback (MakeCallback (&DashServer::HandleRead, this));
    // socket->SetDataSentCallback (MakeCallback(&DashServer::SendSegment, this));

    m_socketList.push_back (socket);
    m_fromList.push_back (from);
  }

  void DashServer::HandlePeerClose (Ptr<Socket> socket) {
    NS_LOG_DEBUG ("Local socket closed. \nSERVER RX: " << m_totalRx << " bytes \nSERVER TX " << m_totalTx << " bytes ");
    if (!m_stopped) {
      StopApplication ();
    }
  }

  void DashServer::HandlePeerError (Ptr<Socket> socket) {
    NS_LOG_WARN ("Encountered Problem with peer: " << this << " socket: " << socket);

  }

  void DashServer::SendSegment (Ptr<Socket> socket, uint32_t size) {
    std::ostringstream payloadstr;
    payloadstr << std::to_string(GetCurrentFragment()) << '\0';
    // Ptr<Packet> packet = Create<Packet> ((uint8_t*) payloadstr.str().c_str(), payloadstr.str().length());
    Ptr<Packet> packet = Create<Packet> (GetCurrentFragment());

    // Adding TCP Header
    TcpHeader header;
    packet->AddHeader(header);
    InetSocketAddress from = GetAddressFromSocket (socket);
    uint32_t tx = socket->SendTo (packet, 0, from);

    m_totalTx += tx;
    NS_LOG_FUNCTION ("Sending payload of " << packet-> GetSize () << " bytes " << "to: " << from.GetIpv4 () << ":" << from.GetPort () << "; STATUS: " << GetStatus(tx) << " ("<< tx << ")");
    // PrintRequests();
    RemoveCurrentSegment();
  }

  uint32_t DashServer::GetCurrentFragment (void) {
    return m_requestedFragment.front();
  }

  void DashServer::PrintRequests (void) {
    NS_LOG_DEBUG ("Remaining List (server size=" << m_requestedFragment.size () << "):");
    for (auto segment : m_requestedFragment)
      NS_LOG_DEBUG(segment << "\n");
  }

  void DashServer::AddSegment (int segment) {
    m_requestedFragment.push_back(segment);
  }

  void DashServer::RemoveCurrentSegment (void) {
    m_requestedFragment.pop_front();
  }

  Ptr<Socket> DashServer::GetSocket (void) const {
    NS_LOG_FUNCTION (this);
    return m_socketList.front();
  }

  InetSocketAddress DashServer::GetAddressFromSocket (Ptr<Socket> socket) {
    Address addr;

    socket->GetPeerName (addr);
    return InetSocketAddress::ConvertFrom (addr);
  }

  std::string DashServer::GetStatus(uint32_t status) {
    return status!=-1 ? "OK" : "ERR";
  }
} // End of namespace
