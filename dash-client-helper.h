#ifndef DASH_CLIENT_HELPER_H
#define DASH_CLIENT_HELPER_H

#include <stdint.h>
#include <string>
#include "ns3/object-factory.h"
#include "ns3/address.h"
#include "ns3/attribute.h"
#include "ns3/net-device.h"
#include "ns3/node-container.h"
#include "ns3/application-container.h"
#include "ns3/data-rate.h"

namespace ns3 {

  class DashClientHelper {

  public:
    DashClientHelper (Address remote,
                      uint16_t port,std::string protocol, DataRate cbr);
    void SetAttribute (std::string, const AttributeValue &value);
    ApplicationContainer Install (Ptr<Node> node) const;
    ApplicationContainer Install (std::string nodeName) const;
    ApplicationContainer Install (NodeContainer nodes) const;
    Ptr<Application> InstallPriv (Ptr<Node> node) const;

  private:
    std::string m_protocol;
    Address m_local;
    Address m_remote;
    ObjectFactory m_factory;

  };
}

#endif
