#include "dash-client-helper.h"
#include "dash-client.h"
#include "ns3/inet-socket-address.h"
#include "ns3/packet-socket-address.h"
#include "ns3/uinteger.h"
#include "ns3/names.h"
#include "ns3/string.h"

namespace ns3 {

  DashClientHelper::DashClientHelper (Address remote, uint16_t port, std::string protocol, DataRate cbr) {
    m_factory.SetTypeId (DashClient::GetTypeId ());
    SetAttribute ("RemoteAddress", AddressValue (remote));
    SetAttribute ("Protocol", StringValue (protocol));
    SetAttribute ("RemotePort", UintegerValue (port));
    SetAttribute ("DataRate", DataRateValue (cbr));
  }

  void DashClientHelper::SetAttribute (std::string name, const AttributeValue &value) {
    m_factory.Set (name, value);
  }

  ApplicationContainer DashClientHelper::Install (Ptr<Node> node) const {
    return ApplicationContainer (InstallPriv (node));
  }

  ApplicationContainer DashClientHelper::Install (std::string nodeName) const {
    Ptr<Node> node = Names::Find<Node> (nodeName);
    return ApplicationContainer (InstallPriv (node));
  }

  ApplicationContainer DashClientHelper::Install (NodeContainer nodes) const {
    ApplicationContainer apps;
    for (NodeContainer::Iterator i = nodes.Begin (); i != nodes.End (); ++i) {
      apps.Add (InstallPriv (*i));
    }
    return apps;
  }

  Ptr<Application> DashClientHelper::InstallPriv (Ptr<Node> node) const {
    Ptr<Application> app = m_factory.Create<Application> ();
    node->AddApplication (app);

    return app;
  }

}
