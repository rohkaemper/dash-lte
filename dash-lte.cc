#include <string>
#include <fstream>
#include "ns3/core-module.h"
#include "ns3/point-to-point-module.h"
#include "ns3/internet-module.h"
#include "ns3/applications-module.h"
#include "ns3/network-module.h"
#include "ns3/packet-sink.h"
#include "ns3/rng-seed-manager.h"
#include "ns3/error-model.h"

#include "ns3/point-to-point-helper.h"
#include "ns3/ipv4-global-routing-helper.h"
#include "ns3/log.h"

#include "dash-client.h"
#include "dash-client-helper.h"
#include "dash-server.h"
#include "dash-server-helper.h"

using namespace ns3;

NS_LOG_COMPONENT_DEFINE ("DashStreaming");

int main (int argc, char *argv[]) {
  // Enable logging
    // Prefix Symbol      Meaning
    // LOG_PREFIX_FUNC    Prefix the name of the calling function.
    // LOG_PREFIX_TIME    Prefix the simulation time.
    // LOG_PREFIX_NODE    Prefix the node id.
    // LOG_PREFIX_LEVEL   Prefix the severity level.
    // LOG_PREFIX_ALL     Enable all prefixes.
    // e.g.: LogLevel (LOG_PREFIX_TIME | ...)
  LogComponentEnable("DashStreaming", LogLevel(LOG_LEVEL_DEBUG));
  LogComponentEnable("DashServer", LogLevel (LOG_PREFIX_TIME | LOG_PREFIX_FUNC | LOG_LEVEL_DEBUG));
  // LogComponentEnable("DashClient", LogLevel (LOG_PREFIX_TIME | LOG_PREFIX_FUNC | LOG_LEVEL_DEBUG));
  // Use the line below for OUTPUT to file using: ./waf --run dash-lte 2>&1 | awk '/PBUFFER/{print > "PBUFFER.txt"} ...'
  // ./waf --run dash-lte 2>&1 | awk -F: '/PBUFFER/{print $2 > "PBUFFER.txt"} /PSTALL/{print $2 > "PSTALL.txt"} /BUFFDIFF/{print $2 > "BUFFDIFF.txt"} /FRAGMENT/ {print $2 > "FRAGMENT.txt"}'
  LogComponentEnable("DashClient", LogLevel (LOG_LEVEL_DEBUG));

  std::string datarate = "5Mbps";
  std::string delay = "5ms";

  CommandLine cl;
  cl.AddValue ("Datarate", "The DataRate for the point-to-point connection.", datarate);
  cl.AddValue ("Delay", "The Delay for the point-to-point connection.", delay);
  cl.Parse (argc, argv);
  Config::SetDefault ("ns3::TcpSocket::SegmentSize", UintegerValue (1448));
  NS_LOG_INFO ("MAIN: Creating Nodes...");
  NodeContainer nodes;
  nodes.Create (2);

  NS_LOG_INFO ("MAIN: Setup network parameters...");
  InternetStackHelper internet;
  internet.Install (nodes);

  PointToPointHelper pointToPoint;
  pointToPoint.SetDeviceAttribute ("DataRate", StringValue (datarate));
  // pointToPoint.SetDeviceAttribute ("Mtu", UintegerValue (1500));
  pointToPoint.SetChannelAttribute ("Delay", StringValue ("5ms"));

  NetDeviceContainer nodeDevices;
  nodeDevices = pointToPoint.Install (nodes);
  // Uncomment this line to enable pcap capture
  // pointToPoint.EnablePcapAll ("dash", true);

  /* Rate Error if uncommented: */
  // Ptr<UniformRandomVariable> uv = CreateObject<UniformRandomVariable> ();
  // // Set this variable to a specific stream
  // uv->SetStream (50);
  // Ptr<RateErrorModel> em = CreateObject<RateErrorModel> ();
  // em->SetRandomVariable (uv);
  // em->SetAttribute ("ErrorRate", DoubleValue (0.0001));
  // em->SetAttribute ("ErrorUnit", StringValue ("ERROR_UNIT_PACKET"));
  // nodeDevices.Get(1)->SetAttribute ("ReceiveErrorModel", PointerValue (em));
  /* End of Error rate */

  Ipv4AddressHelper ipv4Helper;
  ipv4Helper.SetBase ("10.1.1.0", "255.255.255.0");
  Ipv4InterfaceContainer interfaces = ipv4Helper.Assign (nodeDevices);

  uint16_t serverPort = 8080;
  Address serverAddress (InetSocketAddress (interfaces.GetAddress (0), serverPort));

  NS_LOG_INFO("MAIN: Start Server Applicationon on Node: " << nodes.Get (0));
  DashServerHelper dashServerHelper (serverAddress, serverPort, "ns3::TcpSocketFactory", DataRate(datarate));
  ApplicationContainer serverContainer = dashServerHelper.Install (nodes.Get (0));

  NS_LOG_INFO("MAIN: Start Client Application on Node: " << nodes.Get (1));
  DashClientHelper dashClientHelper (serverAddress, serverPort, "ns3::TcpSocketFactory", DataRate(datarate));
  ApplicationContainer clientContainer = dashClientHelper.Install (nodes.Get (1));

  /* Start Simulation */
  serverContainer.Start (Seconds (0.1));
  clientContainer.Start (Seconds (0.3));
  serverContainer.Stop (Seconds (9000.91));
  clientContainer.Stop (Seconds (9000.92));

  Simulator::Stop (Seconds (9001.0));
  Simulator::Run ();
  Simulator::Destroy ();
}
