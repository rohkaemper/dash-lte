#ifndef DASH_SERVER_H
#define DASH_SERVER_H

#include "ns3/address.h"
#include "ns3/applications-module.h"
#include "ns3/ptr.h"
#include "ns3/traced-callback.h"
#include "ns3/tcp-header.h"
#include "ns3/data-rate.h"
#include "ns3/inet-socket-address.h"
#include <string>
#include <cstdint>

namespace ns3 {

  class Socket;
  class Packet;

  class DashServer : public Application {

  public:
    static TypeId GetTypeId (void);
    DashServer (void);
    virtual ~DashServer (void);
  protected:
    virtual void DoDispose (void);

  private:
    virtual void StartApplication (void);
    virtual void StopApplication (void);
    void HandleRead (Ptr<Socket>);
    void HandleAccept (Ptr<Socket>, const Address&);
    void HandlePeerClose (Ptr<Socket>);
    void HandlePeerError (Ptr<Socket>);
    void SendSegment (Ptr<Socket>, uint32_t);
    uint32_t GetCurrentFragment (void);
    void PrintRequests (void);
    void AddSegment (int);
    void RemoveCurrentSegment (void);
    std::string GetStatus(uint32_t);
    Ptr<Socket> GetSocket (void) const;
    InetSocketAddress GetAddressFromSocket (Ptr<Socket>);
//   void AnswerClientRequest();
//   void ReceiveClientRequest();

    bool            m_stopped;
    Address         m_local;
    Ptr<Socket>     m_socket;
    std::list<Ptr<Socket> > m_socketList;
    std::list<Address> m_fromList;
    uint16_t        m_port;
    uint64_t        m_totalRx;
    uint64_t        m_totalTx;
    DataRate        m_cbrRate;
    TypeId          m_tid;
    std::list<uint32_t>  m_requestedFragment;

    TracedCallback<Ptr<const Packet>, const Address &> m_rxTrace;
    TracedCallback<Ptr<const Packet>, const Address &> m_txTrace;
  };

} // End of namespace
#endif
